module.exports.parse = async (raw, { axios, yaml, notify, console }, { name, url, interval, selected }) => {
    // 解析原配置文件
    const obj = yaml.parse(raw)
    // 创建一个新的代理组
    const hkGroup = {
      name: "香港（负载均衡）", // 代理组名称
      type: "load-balance", // 代理组类型
      url: "http://www.gstatic.com/generate_204",
      proxies: [], // 代理组节点
      interval: 600, // 间隔600s检测一次
      history: 10, // 记录近10次负载
      strategy: "consistent-hashing",
      feedback: true //使用反馈机制，根据延迟和错误率动态调整权重，避免不稳定节点
    }
    const xgpGroup = {
      name: "新加坡（负载均衡）", // 代理组名称
      type: "load-balance", // 代理组类型
      url: "http://www.gstatic.com/generate_204",
      proxies: [], // 代理组节点
      interval: 600,
      history: 10,
      strategy: "consistent-hashing",
      feedback: true
    }
    const twGroup = {
      name: "台湾（负载均衡）",
      type: "load-balance",
      url: "http://www.gstatic.com/generate_204",
      proxies: [],
      interval: 600,
      history: 10,
      strategy: "consistent-hashing",
      feedback: true
    }
    const ytGroup = {
      name: "亚太（负载均衡）",
      type: "load-balance",
      url: "http://www.gstatic.com/generate_204",
      proxies: [],
      interval: 600,
      history: 10,
      strategy: "consistent-hashing",
      feedback: true
    }
    const jpGroup = {
      name: "日韩（负载均衡）",
      type: "load-balance",
      url: "http://www.gstatic.com/generate_204",
      proxies: [],
      interval: 600,
      history: 10,
      strategy: "consistent-hashing",
      feedback: true
    }
    const ytfbGroup = {
      name: "亚太（故障转移）",
      type: "fallback",
      url: "http://www.gstatic.com/generate_204",
      proxies: ["香港（负载均衡）","新加坡（负载均衡）","台湾（负载均衡）","日韩（负载均衡）","亚太（负载均衡）"],
      interval: 600
    }
    const usGroup = {
      name: "美国（负载均衡）",
      type: "load-balance",
      url: "http://www.gstatic.com/generate_204",
      proxies: [],
      interval: 600,
      history: 10,
      strategy: "consistent-hashing",
      feedback: true
    }
    const aiGroup = {
      name: "美新（负载均衡）",
      type: "load-balance",
      url: "http://www.gstatic.com/generate_204",
      proxies: [],
      interval: 600,
      history: 10,
      strategy: "consistent-hashing",
      feedback: true
    }
    const aifbGroup = {
      name: "AI（故障转移）",
      type: "fallback",
      url: "http://www.gstatic.com/generate_204",
      proxies: ["新加坡（负载均衡）","美国（负载均衡）","美新（负载均衡）","台湾（负载均衡）","日韩（负载均衡）"],
      interval: 600
    }
    const euGroup = {
      name: "其他欧美（手动选择）",
      type: "select",
      url: "http://www.gstatic.com/generate_204",
      proxies: [],
      interval: 600
    }
    const manualGroup = {
      name: "单节点（手动选择）",
      type: "select",
      url: "http://www.gstatic.com/generate_204",
      proxies: [],
      interval: 600
    }
    const tipGroup = {
      name: "",
      type: "url-test",
      url: "http://www.gstatic.com/generate_204",
      proxies: [],
      interval: 600000
    }
    const date = new Date();
    const month = date.getMonth() + 1; // 获取月份，注意需要加1
    const day = date.getDate(); // 获取日期
    const hour = date.getHours().toString().padStart(2, '0'); // 获取小时
    const minute = date.getMinutes().toString().padStart(2, '0'); // 获取分钟
    const groupName = `上次更新：${month}月${day}日_${hour}:${minute}`; // 拼接代理组名称
    const timeGroup = { // 创建代理组对象
      name: groupName,
      type: 'url-test',
      url: "http://www.gstatic.com/generate_204",
      proxies: [],
      interval: 600000
    };

    // 遍历原配置文件中的代理节点
    for (const proxy of obj.proxies) {
      if (!(proxy.name.includes("剩余流量")||proxy.name.includes("下次重置")||proxy.name.includes("套餐到期")||proxy.name.includes("跳转域名"))){
        manualGroup.proxies.push(proxy.name) // 全部添加到单节点
      }
      // 如果节点名称包含“香港”或“HKG”，则将其添加到新代理组中
      if (proxy.name.includes("香港") || proxy.name.includes("HKG")) {
        hkGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("新加坡")) {
        xgpGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("台湾")) {
        twGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("台湾")||proxy.name.includes("新加坡")||proxy.name.includes("香港")||proxy.name.includes("HKG")) {
        ytGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("日本")||proxy.name.includes("韩国")) {
        jpGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("美国")) {
        usGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("美国")||proxy.name.includes("新加坡")) {
        aiGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("英国")||proxy.name.includes("西班牙")||proxy.name.includes("荷兰")||proxy.name.includes("德国")||proxy.name.includes("意大利")||proxy.name.includes("土耳其")||proxy.name.includes("俄罗斯")||proxy.name.includes("印度")||proxy.name.includes("加拿大")) {
        euGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("印度")||proxy.name.includes("英国")||proxy.name.includes("德国")||proxy.name.includes("意大利")||proxy.name.includes("荷兰")) {
        aifbGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("剩余流量")) {
        tipGroup.name = proxy.name.replace(' ','_');
      }
      if (proxy.name.includes("下次重置")) {
        tipGroup.proxies.push(proxy.name)
      }
      if (proxy.name.includes("套餐到期")) {
        timeGroup.proxies.push(proxy.name)
        twGroup.proxies.push(proxy.name)
      }
    }
    const resetday = {
      name: "预计今天将进行重置",
      type: "ss",
      server: "1.1.1.1",
      port: 53,
      cipher: "aes-128-gcm",
      password: "password",
      udp: true
    }
    obj["proxies"].push(resetday)
    if (tipGroup.proxies.length < 1) {
      tipGroup.proxies.push("预计今天将进行重置")
    }
    // 将新代理组添加到原配置文件的代理组数组的末尾
    obj["proxy-groups"].splice(1, 0, euGroup, tipGroup, timeGroup)
    obj["proxy-groups"].push(ytfbGroup)
    obj["proxy-groups"].push(aifbGroup)
    obj["proxy-groups"].push(hkGroup)
    obj["proxy-groups"].push(xgpGroup)
    obj["proxy-groups"].push(twGroup)
    obj["proxy-groups"].push(jpGroup)
    obj["proxy-groups"].push(ytGroup)
    obj["proxy-groups"].push(usGroup)
    obj["proxy-groups"].push(aiGroup)
    obj["proxy-groups"].push(manualGroup)

    // 在第一个代理组中添加新代理组的选项
    const group = obj["proxy-groups"][0]
    group.proxies.splice(0, 0, "AI（故障转移）", "亚太（故障转移）", "香港（负载均衡）","新加坡（负载均衡）","台湾（负载均衡）","日韩（负载均衡）","美国（负载均衡）","其他欧美（手动选择）","单节点（手动选择）")
    obj["proxy-groups"].find(group => group.name === "故障转移").proxies.splice(0,0,"AI（故障转移）","亚太（故障转移）","其他欧美（手动选择）")
    //const newProxies = ["香港（负载均衡）","新加坡（负载均衡）","台湾（负载均衡）","亚太（负载均衡）","日韩（负载均衡）","美国（负载均衡）","AI可用（负载均衡）","其他欧美（手动选择）"]
    //group.proxies = group.proxies.slice(0, 2).concat(newProxies, group.proxies.slice(2))

    // 返回处理后的配置文件
    return yaml.stringify(obj)
  }