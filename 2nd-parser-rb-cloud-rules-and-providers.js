module.exports.parse = async (raw, { axios, yaml, notify, console }, { name, url, interval, selected }) => {
    const obj = yaml.parse(raw) // 解析原配置文件
    const mixRuleProviders = { // 定义一个对象，包含所有的规则提供者
        reject: { // 广告域名列表
          type: 'http',
          behavior: 'domain',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/reject.txt',
          path: './ruleset/reject.yaml',
          interval: 86400
        },
        icloud: { // iCloud 域名列表
          type: 'http',
          behavior: 'domain',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/icloud.txt',
          path: './ruleset/icloud.yaml',
          interval: 86400
        },
        apple: { // Apple 在大陆可直连的域名
          type: 'http',
          behavior: 'domain',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/apple.txt',
          path: './ruleset/apple.yaml',
          interval: 86400
        },
        google: { // google可直连的列表
          type: 'http',
          behavior: 'domain',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/google.txt',
          path: './ruleset/google.yaml',
          interval: 86400
        },
        proxy: { // 代理域名列表
          type: 'http',
          behavior: 'domain',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/proxy.txt',
          path: './ruleset/proxy.yaml',
          interval: 86400
        },
        direct: { // 直连域名列表
          type: 'http',
          behavior: 'domain',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/direct.txt',
          path: './ruleset/direct.yaml',
          interval: 86400
        },
        private: { // 私有网络专用域名列表
          type: 'http',
          behavior: 'domain',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/private.txt',
          path: './ruleset/private.yaml',
          interval: 86400
        },
        gfw: { // GFWList 域名列表
          type: 'http',
          behavior: 'domain',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/gfw.txt',
          path: './ruleset/gfw.yaml',
          interval: 86400
        },
        "tld-not-cn": { // 非大陆使用的顶级域名列表
          type: 'http',
          behavior: 'domain',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/tld-not-cn.txt',
          path: './ruleset/tld-not-cn.yaml',
          interval: 86400
        },
        telegramcidr: { // Tg 使用的 IP 地址列表
          type: 'http',
          behavior: 'ipcidr',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/telegramcidr.txt',
          path: './ruleset/telegramcidr.yaml',
          interval: 86400
        },
        cncidr: { // 中国大陆 IP 地址列表
          type: 'http',
          behavior: 'ipcidr',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/cncidr.txt',
          path: './ruleset/cncidr.yaml',
          interval: 86400
        },
        lancidr: { // 局域网 IP 及保留 IP 地址列表
          type: 'http',
          behavior: 'ipcidr',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/lancidr.txt',
          path: './ruleset/lancidr.yaml',
          interval: 86400
        },
        applications: { // 需要直连的常见软件
          type: 'http',
          behavior: 'classical',
          url: 'https://cdn.jsdelivr.net/gh/Loyalsoldier/clash-rules@release/applications.txt',
          path: './ruleset/applications.yaml',
          interval: 86400
        }
      }
    obj['rule-providers'] = mixRuleProviders // 使上面这个对象写入原配置文件中的规则提供者字段
    // 定义复用的规则组名称 string
    const main_group = 'NCloud'
    const hkgroup = '香港（负载均衡）'
    const asia_group = '亚太（故障转移）'
    const ai_group = 'NCloud' //'AI（故障转移）'
    const us_group = '美国（负载均衡）'
    const manual_group = '单节点（手动选择）'
    const openai_group = '美国（负载均衡）'

    // 自定义优先匹配的规则，等同于用户自定义PAC规则
    const prependRules = [
    // Look Up (for Wikipedia@macOS)
    // 'PROCESS-NAME,LookupViewService,' + main_group,
    // 'PROCESS-NAME,/System/Library/PrivateFrameworks/Lookup.framework/Versions/A/XPCServices/LookupViewService.xpc/Contents/MacOS/LookupViewService,' + main_group,
    // 'DOMAIN,lookup-api.apple.com,' + main_group,
    // 'DOMAIN,lookup-api.apple.com.edgekey.net,' + main_group,
    // 'DOMAIN,e16991.b.akamaiedge.net,' + main_group,
    //'DOMAIN,stocks-data-service.apple.com,DIRECT',
    //'DOMAIN,configuration.ls.apple.com,香港（负载均衡）',
    //'DOMAIN,dispatcher.is.autonavi.com,香港（负载均衡）',
    //'DOMAIN-KEYWORD,ssl.ls.apple.com,香港（负载均衡）',
    //'DOMAIN-SUFFIX,smoot.apple.com,香港（负载均衡）',
    //'DOMAIN,amp-api.apps.apple.com,香港（负载均衡）',

    // Windows AI App: Do it locally !
    // - PROCESS-NAME,msedge.exe,AI（故障转移）
    // - PROCESS-NAME,BingWallpaperApp.exe,AI（故障转移）

    // MacOS AI App
    'PROCESS-NAME,Microsoft Edge Canary,' + us_group,
    'PROCESS-NAME,Microsoft Edge Helper,' + us_group,
    'PROCESS-NAME,Microsoft Edge,' + us_group,
    'PROCESS-NAME,ChatGPT,' + openai_group,
    'PROCESS-NAME,Logi AI Prompt Builder,' + openai_group,
    'PROCESS-NAME,Perplexity,' + ai_group,

    // Windows AI domain
    // 'DOMAIN,go.microsoft.com,' + ai_group,
    'DOMAIN-SUFFIX,microsoft.com,' + ai_group,
    'DOMAIN-SUFFIX,msn.com,' + ai_group,
    'DOMAIN-SUFFIX,msn.cn,' + ai_group,
    'DOMAIN-SUFFIX,cn.bing.com,' + 'DIRECT', // Exception for CN Bing
    'DOMAIN-SUFFIX,bing.com,' + ai_group,
    'DOMAIN-SUFFIX,msftstatic.com,' + ai_group,
    'DOMAIN-SUFFIX,akamaized.net,' + ai_group,
    'DOMAIN,bard.google.com,' + us_group,
    'DOMAIN,gemini.google.com,' + us_group,
    'DOMAIN,mtalk.google.com,' + us_group,
    'DOMAIN-SUFFIX,google-analytics.com,' + ai_group,

    // AI Domain
    // 'DOMAIN,www.bing.com,' + ai_group,
    // 'DOMAIN,global.bing.com,' + ai_group,
    'DOMAIN-SUFFIX,perplexity.ai,' + manual_group,
    
    // OpenAI
    'DOMAIN-KEYWORD,openai,' + openai_group,
    'DOMAIN-SUFFIX,chatgpt.com,' + openai_group,
    'DOMAIN-SUFFIX,openai.com,' + openai_group,
    'DOMAIN-SUFFIX,oaistatic.com,' + openai_group,
    'DOMAIN-SUFFIX,featureassets.org,' + openai_group,

    'DOMAIN-SUFFIX,claude.ai,' + ai_group,
    'DOMAIN-KEYWORD,anthropic,' + us_group,
    'PROCESS-NAME,QuickGPT,' + ai_group,
    // 'DOMAIN,copilot.microsoft.com,' + ai_group,
    // 'DOMAIN-SUFFIX,edge.microsoft.com,' + ai_group,
    'DOMAIN-SUFFIX,login.microsoftonline.com,' + ai_group,

    // Gerneral Domain
    'DOMAIN-SUFFIX,wikiwand.com,' + main_group,
    'DOMAIN-SUFFIX,logseq.com,' + main_group,
    'DOMAIN-KEYWORD,githubusercontent,' + main_group,
    'DOMAIN-KEYWORD,gitlab,' + main_group,

    'DOMAIN-SUFFIX,focusmedica.com,' + main_group,
    'DOMAIN-SUFFIX,khanacademy.org,' + main_group,
    'DOMAIN-SUFFIX,omnivore.app,' + main_group,
    'DOMAIN-SUFFIX,nerdfonts.com,' + main_group,
    'DOMAIN-SUFFIX,republicworld.com,' + main_group,
    'DOMAIN-SUFFIX,tampermonkey.net,' + main_group,
    'DOMAIN-SUFFIX,engadget.com,' + main_group,
    'DOMAIN-SUFFIX,rsshub.app,' + main_group,
    'DOMAIN-SUFFIX,vultr.com,' + main_group,
    'DOMAIN-SUFFIX,lovense.com,' + main_group,
    'DOMAIN-SUFFIX,reddit.com,' + main_group,

    'DOMAIN-KEYWORD,torrentgalaxy,' + main_group,
    'DOMAIN-SUFFIX,btdig.com,' + main_group,
    'DOMAIN-SUFFIX,vipor.net,' + main_group,
    'DOMAIN-SUFFIX,unusualporn.com,' + main_group,
    'DOMAIN-SUFFIX,hypnoladies.com,' + main_group,
    'DOMAIN-SUFFIX,wearlatex.com,' + main_group,

    // HK 优先
    'DOMAIN-SUFFIX,appstorrent.ru,' + hkgroup,
    'DOMAIN-SUFFIX,userapi.com,'+ hkgroup,

    // Asia 可用&优先
    'DOMAIN-KEYWORD,shibbydex,' + asia_group,
    'DOMAIN-SUFFIX,binance.com,' + asia_group,
    'DOMAIN-KEYWORD,giphy,' + asia_group,
    'DOMAIN-SUFFIX,statementdog.com,' + asia_group,

    // DIRECT for CN App & Domain
    'DOMAIN-SUFFIX,jianguoyun.com,DIRECT',
    'DOMAIN-KEYWORD,libvio,DIRECT',
    'DOMAIN,p2.cfnode1.xyz,DIRECT',
    'DOMAIN-SUFFIX,maofan.top,DIRECT',
    'DOMAIN-SUFFIX,lz-cdn4.com,DIRECT',
    // DIRECT Domain
    'DOMAIN-SUFFIX,gmx.com,DIRECT',
    // 对接 Clash 的 Web GUI
    'DOMAIN,clash.razord.top,DIRECT',
    'DOMAIN,yacd.haishan.me,DIRECT',
    'DOMAIN,smzdm.com,DIRECT',
    // Windows Usual Direct Program
    'PROCESS-NAME,WeChatOCR.exe,DIRECT', // 微信
    'PROCESS-NAME,WeChat.exe,DIRECT',
    'PROCESS-NAME,WeChatPlayer.exe,DIRECT',
    'PROCESS-NAME,WeChatAppEx.exe,DIRECT',
    'PROCESS-NAME,WeChatUtility,DIRECT',
    'PROCESS-NAME,qbitorrent.exe,DIRECT',
    'PROCESS-NAME,Bypass.exe,DIRECT',
    'PROCESS-NAME,TdxW.exe,DIRECT', // 通达信
    'PROCESS-NAME,tdxcef.exe,DIRECT',
    'PROCESS-NAME,cs2.exe,DIRECT', // CS
    'PROCESS-NAME,ToDesk.exe,DIRECT', // ToDesk
    'PROCESS-NAME,cloudmusic.exe,DIRECT', // 网易云音乐
    'PROCESS-NAME,cloudmusic_reporter.exe,DIRECT',
    'PROCESS-NAME,cloudmusic_util.exe,DIRECT',
    'PROCESS-NAME,CrashReporter.exe,DIRECT',
    'PROCESS-NAME,minidump_stackwalk.exe,DIRECT',
    'PROCESS-NAME,NutstoreClient.exe,DIRECT', // 坚果云
    'PROCESS-NAME,desktopcal.exe,DIRECT', // 日历
    'PROCESS-NAME,dkdockhost.exe,DIRECT',
    // Windows Reject Program
    'PROCESS-NAME,coodesker-x64.exe,REJECT',
    // MacOS Usual Direct Program
    'PROCESS-NAME,qbitorrent,DIRECT',
    'PROCESS-NAME,WeChat,DIRECT',
    'DOMAIN-SUFFIX,weixin.com,DIRECT',
    'PROCESS-NAME,欧路词典,DIRECT',
    'PROCESS-NAME,EuDic LightPeek,DIRECT',
    // MacOS Usual Global Program
    'PROCESS-NAME,VibeMate,' + us_group,
    'PROCESS-NAME,VibeMate Helper,' + us_group,

    // 提前应用的规则集，位于自定义规则之后，配置文件规则之前
    'RULE-SET,applications,DIRECT',
    'RULE-SET,private,DIRECT',
    'RULE-SET,reject,REJECT',
    'RULE-SET,apple,DIRECT',
    'RULE-SET,icloud,DIRECT',
    'RULE-SET,tld-not-cn,' + main_group,
    'RULE-SET,gfw,' + main_group,
    'RULE-SET,telegramcidr,' + main_group,
    // 'RULE-SET,proxy,' + main_group,
    'RULE-SET,direct,DIRECT',
    'RULE-SET,lancidr,DIRECT',
    'RULE-SET,cncidr,DIRECT',

    ]

    obj['rules'] = prependRules.concat(obj['rules']) // 将上面这个数组合并到原配置文件中的rules属性前面
    return yaml.stringify(obj)  // 将obj对象字段化，返回处理后的配置文件
    }